import rssActivityTracker
import unittest
from dateutil.parser import parse
import datetime

class TestRSSActivityTracker(unittest.TestCase):
    def testParseRSSFeedData(self):
        rssFeedsDict = rssActivityTracker.parseRSSFeedData('./mockData/rssFeedsGood.json')

        self.assertEqual(len(rssFeedsDict), 8)
    
    def testParseRSSFeedDataBad(self):
        rssFeedsDict = None
        try:
            rssFeedsDict = rssActivityTracker.parseRSSFeedData('badPath')
            self.fail()
        except FileNotFoundError:
            self.assertIsNone(rssFeedsDict)

    def testParseRSSFeedXML(self):
        # Test parsing a sample static RSS feed
        rssFeedData = rssActivityTracker.parseRSSFeedXML('https://www.feedforall.com/sample.xml')

        self.assertEqual(rssFeedData.find('channel').find('title').text, 'FeedForAll Sample Feed')

    def testParseRSSFeedXMLBad(self):
        rssFeedData = None
        try:
            rssFeedData = rssActivityTracker.parseRSSFeedXML('badPath')
        except Exception:
            self.assertIsNone(rssFeedData)

    def testGetLastUpdatedDate(self):
        rssFeedData = rssActivityTracker.parseRSSFeedXML('https://www.feedforall.com/sample.xml')
        lastUpdatedDate = rssActivityTracker.getLastUpdatedDate(rssFeedData)

        self.assertEqual(lastUpdatedDate, parse('Tue, 19 Oct 2004 13:39:14 -0400'))

    def testGetLastUpdatedDateBad(self):
        lastUpdatedDate = None
        try:
            lastUpdatedDate = rssActivityTracker.getLastUpdatedDate(None)
        except Exception:
            self.assertIsNone(lastUpdatedDate)

    def testIsFeedOutOfDate(self):
        datetime1 = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(days=5)

        self.assertEqual(rssActivityTracker.isFeedOutOfDate(datetime1, 7), False)
        self.assertEqual(rssActivityTracker.isFeedOutOfDate(datetime1, 5), True)
        self.assertEqual(rssActivityTracker.isFeedOutOfDate(datetime1, 2), True)

    def testIsFeedOutOfDateBad(self):
        isOutOfDate = None
        try:
            isOutOfDate = rssActivityTracker.isFeedOutOfDate(None, 7)
        except Exception:
            self.assertIsNone(isOutOfDate)

if __name__ == '__main__':
    unittest.main()