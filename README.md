# RSS Activity Tracker
Given an input with RSS feeds, determines which feeds have had no activity for a given number of days.

This is a command-line application. A dictionary of RSS companies and feeds is provided in `rssFeeds.json` and can be updated to add or remove RSS feeds. The number of days is specified with a command-line argument.

Since a dictionary is being used, company names should be unique. For companies that have multiple feeds, a descriptive name should be added alongside the company name. For a feed to have no activity, there should not be anything published within the given number of days.

## How To Use
```bash
git clone git@gitlab.com:WumpaFruit/rss-activity-tracker.git
cd rss-activity-tracker
pip install requests python-dateutil
python rssActivityTracker.py # Use default of 7 days
python rssActivityTracker.py 14 # Specify number of days
python rssActivityTrackerTest.py # Run unit tests
```