import sys
import json
import requests
import xml.etree.ElementTree as ET 
from dateutil.parser import parse
import datetime
from dateutil.relativedelta import relativedelta

DEFAULT_NUMBER_OF_DAYS = 7
DEFAULT_RSS_FEED_JSON_PATH = 'rssFeeds.json'

def parseRSSFeedData(rssFeedJsonPath=DEFAULT_RSS_FEED_JSON_PATH):
    """
    Parses the JSON file containing the RSS feed data and returns its object representation.
    """
    try:
        with open(rssFeedJsonPath, 'r') as f:
            return json.load(f)
    except IOError as error:
        print('Error while parsing RSS feed data:')
        print(error)
        raise error

def parseRSSFeedXML(rssFeedURL):
    """
    Parses the XML file from the given URL and returns its object representation.
    """
    try:
        resp = requests.get(rssFeedURL)
        root = ET.fromstring(resp.content)
        return root
    except Exception as error:
        print(error)
        raise error

def getLastUpdatedDate(xmlData):
    """
    Calculates the last updated date from the given XML data.
    """
    try:
        lastBuildDate = xmlData.find('channel').find('lastBuildDate')

        if (lastBuildDate is not None):
            # Use the lastBuildDate tag if it exists
            return parse(lastBuildDate.text)
        else:
            # Find the most recent pubDate of an article
            lastPubDate = datetime.datetime(datetime.MINYEAR, 1, 1, tzinfo=datetime.timezone.utc)
            for item in xmlData.find('channel').findall('item'):
                pubDateText = item.find('pubDate').text
                pubDate = parse(pubDateText)
                if pubDate > lastPubDate:
                    lastPubDate = pubDate
            return lastPubDate
    except Exception as error:
        print(error)
        raise error

def isFeedOutOfDate(lastUpdatedDate, numberOfDays):
    """
    Returns whether the given date is more than the given number of days old
    """
    try:
        now = datetime.datetime.now(datetime.timezone.utc)
        diff = now - lastUpdatedDate

        return diff.days >= numberOfDays
    except Exception as error:
        print(error)
        raise error

if __name__ == '__main__':
    try:
        if (len(sys.argv) == 2):
            numberOfDays = int(sys.argv[1])
        else:
            numberOfDays = DEFAULT_NUMBER_OF_DAYS
        rssFeedsDict = parseRSSFeedData()
        
        outOfDateFeeds = []
        for index, rssFeed in enumerate(rssFeedsDict):
            try:
                rssFeedURL = rssFeedsDict[rssFeed]
                print("\nChecking " + rssFeed + " (" +  str(index + 1) + "/" + str(len(rssFeedsDict)) + ")")
                rssFeedData = parseRSSFeedXML(rssFeedURL)
                lastUpdatedDate = getLastUpdatedDate(rssFeedData)

                if (isFeedOutOfDate(lastUpdatedDate, numberOfDays)):
                    outOfDateFeeds.append(rssFeed)
            except Exception as error:
                print("Could not process RSS feed XML data")

        if (len(outOfDateFeeds) is 0):
            print("\nNo feeds are out of date!")
        else:
            print("\nHere are the feeds that are at least " + str(numberOfDays) + " day(s) old:")
            for outOfDateRSSFeed in outOfDateFeeds:
                print(outOfDateRSSFeed)
    except Exception as error:
        print('Could not process RSS feed data')